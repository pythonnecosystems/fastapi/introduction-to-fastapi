# FastAPI 개론 

## API란?

![](./images/0_tx-iYU8L50ApJ_EH.webp)

식당에 가서 음식을 주문한다고 상상해 보자. 원하는 음식을 먹기 위해 웨이터에게 말을 걸어 주문 내용을 말한다. 그러면 웨이터가 주문을 주방으로 가져가고 셰프는 요청에 따라 음식을 준비한다. 마지막으로 웨이터가 음식을 다시 가져다주면 식사를 즐길 수 있다.

이 시나리오에서 웨이터는 API(Application Programming Inerface)와 같다. API는 웨이터와 소통하여 주문한 음식을 주방에 가져다주는 것처럼 서로 다른 소프트웨어 어플리케이션이 통신하고 정보를 교환할 수 있는 방법이다.

API를 사용하면 사람 대신 서로 다른 컴퓨터 프로그램, 앱 또는 웹사이트가 서로 대화하고 데이터를 공유할 수 있다. 예를 들어, 모바일 앱을 사용하여 날씨를 확인하면 앱은 API를 사용하여 날씨 서비스에서 현재 날씨 정보를 가져온다. 앱은 날씨 데이터를 요청하는 요청을 API에 보내고, API는 요청된 정보를 응답한다.

API를 사용하면 서로 다른 앱과 서비스가 함께 작동하고 정보를 원활하게 공유할 수 있다. API는 한 프로그램이 다른 프로그램에 데이터를 요청하는 방법과 데이터가 다시 전송되는 방법을 지정하는 일련의 규칙과 프로토콜을 정의한다.

API는 서로 다른 애플리케이션이 서로를 이해하고 협업할 수 있는 언어라고 생각할 수 있다. 이를 통해 개발자는 다른 서비스에서 제공하는 기능을 활용하여 새로운 앱을 만들거나 기존 앱을 통합할 수 있다.

따라서 API는 서로 다른 소프트웨어 어플리케이션이 서로 대화하고, 데이터를 교환하고, 협력하여 디지털 세상에서 필요한 서비스와 정보를 제공할 수 있도록 도와주는 메신저와 같다.

## FastAPI란?

![](./images/0_ByXDc1gMKPIE1tm5.webp)

FastAPI는 API를 빠르고 쉽게 구축할 수 있도록 도와주는 Python 웹 프레임워크이다. 인터넷을 통해 데이터를 주고받을 수 있는 웹 어플리케이션을 간단하게 만들 수 있는 일련의 도구와 기능을 제공한다.

위에서 설명한 바와 같이 API는 서로 다른 어플리케이션이 서로 대화할 수 있는 방법이라고 생각하면 된다. 예를 들어, 모바일 앱이 인터넷에서 데이터를 가져올 때 해당 정보를 가져오기 위해 API를 사용하고 있을 것이다. FastAPI는 다른 앱이 사용할 수 있는 데이터와 서비스를 제공하는 어플리케이션의 일부를 구축하는 데 도움을 준다.

FastAPI는 이름에서 알 수 있듯이 빠르고 효율적으로 설계되었다. 비동기 프로그래밍이라는 최신 접근 방식을 사용하여 여러 작업을 서로 차단하지 않고 동시에 실행할 수 있다. 즉, 여러 사용자의 많은 요청을 속도 저하 없이 처리할 수 있다.

FastAPI는 사용자 친화적인 인터페이스도 갖추고 있다. 웹 개발 초보자도 쉽게 이해하고 작업할 수 있도록 설계되었다. 단순성과 가독성으로 유명한 인기 프로그래밍 언어인 Python을 사용한다. FastAPI를 사용하면 깔끔하고 체계적인 코드를 작성하여 API를 구축할 수 있다.

FastAPI의 멋진 점 중 하나는 작성하는 코드를 기반으로 API에 대한 문서를 자동으로 생성한다는 것이다. 이 문서는 다른 개발자에게 API 사용 방법과 예상되는 데이터를 이해하는 데 도움을 준다. 직접 작성하지 않고도 API에 대한 설명서가 있는 것과 같다.

전반적으로 FastAPI는 API 구축 과정을 간소화하는 강력한 도구이다. 다른 앱과 통신하고, 여러 요청을 효율적으로 처리하고, 문서를 자동으로 생성할 수 있는 웹 어플리케이션을 만드는 데 도움이 된다. Python을 사용하여 API를 빠르고 쉽게 만들고 싶다면 이 도구는 훌륭한 선택일 것이다.

## 비동기 프로그래밍이란?

![](./images/0_gSk48A6n7QK5HZcA.webp)

숙제나 집안일 등 해야 할 일이 산더미처럼 쌓여 있다고 상상해 보자. 일반적으로는 한 번에 한 가지 작업을 완료한 다음 다음 작업으로 넘어간다. 하지만 웹 페이지가 로드될 때까지 기다리거나 대용량 파일을 다운로드하는 등 완료하는 데 시간이 오래 걸리는 작업이 있을 때가 있다. 이는 마치 다음 작업으로 넘어가기 전에 어떤 일이 일어나기를 기다리는 것과 같다.

이제 이러한 느린 작업이 완료되기를 기다리는 동안 다른 작업을 할 수 있다고 상상해 보자. 예를 들어, 웹 페이지가 로드되기를 기다리는 동안 다른 과제 작업을 시작하거나 휴대폰으로 간단한 게임을 하는 등 재미있는 일을 할 수 있다. 이렇게 하면 한 작업이 완료되기를 기다리는 동안 다른 작업이 대기하는 시간을 낭비하지 않아도 된다.

Python 프로그래밍 언어를 기반으로 구축된 FastAPI는 비동기 프로그래밍에 대한 Python의 기본 지원을 활용하여 비동기 작업을 처리한다.

FastAPI에서는 `async`와 `await` 키워드를 사용하여 비동기 경로와 함수를 정의할 수 있다. 함수나 경로를 비동기로 표시하면 해당 함수가 비동기 연산을 수행할 수 있음을 나타낸다. 이렇게 하면 전체 어플리케이션을 차단하지 않고 데이터베이스 쿼리나 API 호출과 같은 외부 리소스를 기다리는 동안 함수나 경로의 실행을 일시 중지할 수 있다.

다음은 FastAPI가 비동기 작업, 특히 외부 API에서 비동기로 데이터를 가져오는 작업을 처리하는 방법을 보여주는 보다 실용적인 예이다.

```python
from fastapi import FastAPI
import httpx


app = FastAPI()

async def fetch_data(url):
    async with httpx.AsyncClient() as client:
        response = await client.get(url)
        return response.json()

@app.get("/data")
async def get_data():
    data_url = "https://api.example.com/data"
    data = await fetch_data(data_url)
    return {"data": data}
```

이 예에는 `httpx` 라이브러리를 사용하여 비동기 HTTP GET 요청을 수행하는 `fetch_data`라는 새 함수가 있다. `fetch_data` 함수는 `async` 키워드를 사용하는 코루틴으로 정의되었다.

`get_data` 엔드포인트에서는 `await` 키워드를 사용하여 외부 API에서 비동기로 데이터를 가져오기 위해 `fetch_data` 코루틴을 호출한다. API에서 응답을 받을 때까지 `get_data` 함수의 실행이 일시 중지된다.

이를 통해 FastAPI는 여러 요청을 동시에 처리할 수 있다. 하나의 요청이 외부 API의 응답을 기다리는 동안 FastAPI는 다른 요청을 처리할 수 있으므로 어플리케이션의 효율성과 응답성이 향상된다.

## Fast API를 사용하여 가장 기본적인 경로의 설정과 실행

1. 먼저 디렉토리를 생성하고 디렉토리 내부로 이동한 다음 아래 명령을 실행한다.

```bash
$ python -m venv venv
```

2. 그런 다음 가상 환경을 활성화한다. 이 두 단계는 주로 격리된 환경에서 이 개발을 수행하기 위한 것이다.

```bash
$ source venv/bin/activate
```

3. 종속성과 함께 가상 환경에 FastAPI를 설치한다.

```bash
$ pip install fastapi[all]
```

4. 다음 VSCode 또는 PyCharm을 열고 아래 코드를 파일네 저장하고 생성된 디렉터리에 저장한다.

```python
from fastapi import FastAPI 


app = FastAPI() 

@app.get("/") 
def root(): 
    return {"message": "Hello World from FastAPI"}
```

5. 명령 프롬프트로 이동하여 디렉터리로 이동한 후 다음 명령을 실행한다.

```bash
$ uvicorn main:app --reload
```

이 명령은 :

- `main`: `main.py` 파일(Python "module").
- `app`: `app = FastAPI()`을 사용하여 `main.py` 내부에 생성된 객체.
- `--reload`: 코드 변경 후 서버를 다시 시작하게 한다. 개발용으로만 사용하여야 한다.

```
INFO:     Will watch for changes in these directories: ['C:\\personalwork\\apitutorial\\app']
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
INFO:     Started reloader process [16600] using WatchFiles
INFO:     Started server process [13332]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
INFO:     127.0.0.1:64425 - "GET / HTTP/1.1" 200 OK
```

브라우저를 열고 URL을 방문하면 다음과 같은 JSON 응답을 볼 수 있다.

![](./images/0_dUkxBZRjm5_ip9gZ.webp)

> **부스터 팁**<br>
FastAPI에서는 Python 사전을 JSON으로 자동 변환하는 작업을 프레임워크에서 처리한다. FastAPI는 `fastapi.responses` 모듈의 `JSONResponse` 클래스를 사용하여 JSON 직렬화와 응답 생성을 처리한다.

FastAPI는 엔드포인트 함수의 반환 타입을 자동으로 감지한다. Python 딕셔너리가 반환되면 FastAPI는 이를 JSON 데이터로 인식하고 JSON 문자열로 직렬화한다. 그런 다음 이 JSON 문자열은 적절한 콘텐츠 타입 헤더(application/json)와 함께 HTTP 응답 본문에 포함되어 응답에 JSON 데이터가 포함되어 있음을 나타낸다.

Python 사전을 JSON으로 자동 변환함으로써 FastAPI는 API 엔드포인트에서 구조화된 데이터를 반환하는 프로세스를 간소화한다. 이를 통해 어플리케이션 내부에서 Python 객체와 사전을 작업하는 동시에 클라이언트가 쉽게 전송하고 이해할 수 있는 형식으로 원활하게 변환할 수 있다.

## 대화형 API 문서
자동으로 생성된 대화형 API 설명서(Swagger UI에서 제공)를 참조할 수도 있다(http://127.0.0.1:8000/docs).

![](./images/0_Pv0oeayHLWPH_NMF.webp)


